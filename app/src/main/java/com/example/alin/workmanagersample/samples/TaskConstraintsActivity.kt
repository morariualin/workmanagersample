package com.example.alin.workmanagersample.samples

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import androidx.work.*

class TaskConstraintsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = "Task Constraints"

        val constraints = Constraints.Builder()
                .setRequiresCharging(true)
//                .setRequiredNetworkType(NetworkType.CONNECTED)
//                .setRequiresBatteryNotLow(true)
//                .setRequiresDeviceIdle(true)
//                .setRequiresStorageNotLow(true)
                // other constraints
                .build()
        val work = OneTimeWorkRequest.Builder(CountWorkerWithConstraints::class.java)
                .setConstraints(constraints)
                .build()

        WorkManager.getInstance()?.enqueue(work)

        val workStatusObserver: Observer<WorkStatus> = Observer { workStatus ->
            if (workStatus != null) {
                Log.d("CountWorker", "work status with id ${work.id} = ${workStatus.state}")
            }
        }
        WorkManager.getInstance()?.getStatusById(work.id)
                ?.observe(this, workStatusObserver)
    }
}

class CountWorkerWithConstraints : Worker() {

    override fun doWork(): Result {
        for (i in 0..10) {
            Log.d("CountWorker", "-> $i")
            Thread.sleep(100)
        }

        return Worker.Result.SUCCESS
    }
}