package com.example.alin.workmanagersample.samples

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import androidx.work.*
import com.example.alin.workmanagersample.R
import kotlinx.android.synthetic.main.activity_complex_worker.*

class ChainedTasksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_complex_worker)

        title = "Chained Tasks"

        startSimpleChain.setOnClickListener {
            startSimpleChainedWork()
        }

        startComplexChain.setOnClickListener {
            startComplexChainedWork()
        }

        startMoreComplexChain.setOnClickListener {
            startCombinedChainedWork()
        }
    }

    private fun startSimpleChainedWork() {
        val workAData: Data = mapOf("multiplication" to 1,
                "workName" to "workA")
                .toWorkData()
        val workA = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workAData)
                .build()

        val workBData: Data = mapOf("multiplication" to 2,
                "workName" to "workB")
                .toWorkData()
        val workB = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workBData)
                .build()

        val workCData: Data = mapOf("multiplication" to 3,
                "workName" to "workC")
                .toWorkData()
        val workC = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workCData)
                .build()

        WorkManager.getInstance()?.beginWith(workA)
                ?.then(workB)
                ?.then(workC)
                ?.enqueue()
    }

    private fun startComplexChainedWork() {
        val workAData1: Data = mapOf("multiplication" to 1,
                "workName" to "workA1")
                .toWorkData()
        val workA1 = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workAData1)
                .build()
        val workAData2: Data = mapOf("multiplication" to 1,
                "workName" to "workA2")
                .toWorkData()
        val workA2 = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workAData2)
                .build()
        val workAData3: Data = mapOf("multiplication" to 1,
                "workName" to "workA3")
                .toWorkData()
        val workA3 = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workAData3)
                .build()

        val workBData: Data = mapOf("multiplication" to 2,
                "workName" to "workB")
                .toWorkData()
        val workB = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workBData)
                .build()

        val workCData1: Data = mapOf("multiplication" to 3,
                "workName" to "workC1")
                .toWorkData()
        val workC1 = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workCData1)
                .build()
        val workCData2: Data = mapOf("multiplication" to 3,
                "workName" to "workC2")
                .toWorkData()
        val workC2 = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workCData2)
                .build()

        WorkManager.getInstance()?.beginWith(workA1, workA2, workA3)
                ?.then(workB)
                ?.then(workC1, workC2)
                ?.enqueue()
    }

    private fun startCombinedChainedWork() {
        val workAData: Data = mapOf("multiplication" to 1,
                "workName" to "workA")
                .toWorkData()
        val workA = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workAData)
                .build()

        val workBData: Data = mapOf("multiplication" to 2,
                "workName" to "workB")
                .toWorkData()
        val workB = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workBData)
                .build()

        val workCData: Data = mapOf("multiplication" to 3,
                "workName" to "workC")
                .toWorkData()
        val workC = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workCData)
                .build()

        val workDData: Data = mapOf("multiplication" to 4,
                "workName" to "workD")
                .toWorkData()
        val workD = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workDData)
                .build()

        val workEData: Data = mapOf("multiplication" to 5,
                "workName" to "workE")
                .toWorkData()
        val workE = OneTimeWorkRequestBuilder<ChainedCountWork>()
                .setInputData(workEData)
                .build()

        val chain1 = WorkManager.getInstance()?.beginWith(workA)?.then(workB)
        val chain2 = WorkManager.getInstance()?.beginWith(workC)?.then(workD)

        val chain3 = WorkContinuation.combine(chain1, chain2).then(workE)
        chain3.enqueue()
    }
}

class ChainedCountWork : Worker() {

    override fun doWork(): Result {
        val multiplicationValue = inputData.getInt("multiplication", 0)
        val workName = inputData.getString("workName", "")

        for (i in 0..10) {
            Log.d("CountWorker", "$workName -> ${multiplicationValue * 10 + i}")
            Thread.sleep(100)
        }

        return Worker.Result.SUCCESS
    }
}