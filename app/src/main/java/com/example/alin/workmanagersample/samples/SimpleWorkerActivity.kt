package com.example.alin.workmanagersample.samples

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import androidx.work.*
import com.example.alin.workmanagersample.R
import kotlinx.android.synthetic.main.activity_simple_worker.*

class SimpleWorkerActivity : AppCompatActivity() {

    private var countWork: OneTimeWorkRequest? = null

    private val workStatusObserver: Observer<WorkStatus> = Observer { workStatus ->
        if (workStatus != null) {
            Log.d("CountWorker", "work status with id ${countWork?.id} = ${workStatus.state}")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_simple_worker)

        title = "Simple task"

        startCount.setOnClickListener {
            startSimpleCountWork()
        }

        startRetryCount.setOnClickListener {
            startRetryCountWork()
        }

        stopCount.setOnClickListener {
            stopWorkIfAny()
        }

        checkWorkStatus.setOnClickListener {
            checkWorkStatus()
        }
    }

    private fun startSimpleCountWork() {
        val retryData: Data = mapOf("returnValue" to Worker.Result.SUCCESS.ordinal)
                .toWorkData()
        countWork = OneTimeWorkRequest.Builder(CountWorker::class.java)
                .setInputData(retryData)
                .build()
        // this can be done using OneTimeWorkRequestBuilder<CountWorker>() extension method

        WorkManager.getInstance()?.enqueue(countWork)
    }

    private fun startRetryCountWork() {
        val retryData: Data = mapOf("returnValue" to Worker.Result.RETRY.ordinal)
                .toWorkData()
        countWork = OneTimeWorkRequest.Builder(CountWorker::class.java)
                .setInputData(retryData)
                .build()
        WorkManager.getInstance()?.enqueue(countWork)
    }

    private fun checkWorkStatus() {
        if (countWork != null) {
            WorkManager.getInstance()?.getStatusById(countWork?.id!!)
                    ?.observe(this, workStatusObserver)
        }
    }

    private fun stopWorkIfAny() {
        if (countWork != null) {
            WorkManager.getInstance()?.cancelWorkById(countWork?.id!!)
            countWork = null
        }
    }
}

class CountWorker : Worker() {

    override fun doWork(): Result {
        val returnValue =
                Result.values()[inputData.getInt("returnValue", 1)]

        for (i in 0..10) {
            Log.d("CountWorker", "-> $i")

            Thread.sleep(100)
        }

        outputData = mapOf("outKey" to "outputData").toWorkData()

        return returnValue
    }
}