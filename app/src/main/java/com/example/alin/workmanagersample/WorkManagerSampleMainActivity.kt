package com.example.alin.workmanagersample

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.alin.workmanagersample.samples.*
import kotlinx.android.synthetic.main.activity_work_manager_sample_main.*

class WorkManagerSampleMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_manager_sample_main)

        workManagerList.layoutManager = LinearLayoutManager(this)
        workManagerList.adapter = WorkManagerSampleAdapter(this) {
            when (it) {
                0 -> startActivity(Intent(this, SimpleWorkerActivity::class.java))
                1 -> startActivity(Intent(this, TaskConstraintsActivity::class.java))
                2 -> startActivity(Intent(this, RecurringTasksActivity::class.java))
                3 -> startActivity(Intent(this, ChainedTasksActivity::class.java))
                4 -> startActivity(Intent(this, UniqueWorkSequencesActivity::class.java))
            }
        }
    }
}

class WorkManagerSampleAdapter(context: Context, val onItemClick: ((Int) -> Unit))
    : RecyclerView.Adapter<WorkManagerSampleAdapter.WorkManagerViewHolder>() {

    private val li = LayoutInflater.from(context)
    private val items: List<String> = listOf(
            "Simple task",
            "Task Constraints",
            "Recurring Tasks",
            "Chained Tasks",
            "Unique work sequences")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkManagerViewHolder {
        return WorkManagerViewHolder(li.inflate(android.R.layout.simple_list_item_1, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: WorkManagerViewHolder, position: Int) {
        holder.bindView(position, items[position])
    }

    inner class WorkManagerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(position: Int, text: String) {
            itemView.findViewById<TextView>(android.R.id.text1).text = text

            itemView.setOnClickListener {
                onItemClick.invoke(position)
            }
        }
    }
}