package com.example.alin.workmanagersample.samples

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import androidx.work.*
import com.example.alin.workmanagersample.R
import kotlinx.android.synthetic.main.activity_unique_worker.*

class UniqueWorkSequencesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_unique_worker)

        title = "Unique work sequences"

        startCount.setOnClickListener {
            startSimpleChainedWork()
        }

        stopTaggedWork.setOnClickListener {
            stopTaggedWork()
        }

        startSimpleChainedWork()

    }

    private fun startSimpleChainedWork() {
        val workAData: Data = mapOf("multiplication" to 1,
                "workName" to "workA")
                .toWorkData()
        val workA = OneTimeWorkRequestBuilder<UniqueCountWorker>()
                .setInputData(workAData)
                .build()

        val workBData: Data = mapOf("multiplication" to 2,
                "workName" to "workB")
                .toWorkData()
        val workB = OneTimeWorkRequestBuilder<UniqueCountWorker>()
                .setInputData(workBData)
                .build()

        val workCData: Data = mapOf("multiplication" to 3,
                "workName" to "workC")
                .toWorkData()
        val workC = OneTimeWorkRequestBuilder<UniqueCountWorker>()
                .addTag("workCTag")
                .setInputData(workCData)
                .build()

        // also you can specify existing work policy to APPEND the work to existing one and KEEP to do nothing if there is existing pending work with the same name
        WorkManager.getInstance()?.beginUniqueWork("workA", ExistingWorkPolicy.REPLACE, workA)
                ?.then(workB)
                ?.then(workC)
                ?.enqueue()
    }

    private fun stopTaggedWork() {
        WorkManager.getInstance()?.cancelAllWorkByTag("workCTag")
    }
}

class UniqueCountWorker : Worker() {

    override fun doWork(): Result {
        val multiplicationValue = inputData.getInt("multiplication", 0)
        val workName = inputData.getString("workName", "")

        for (i in 0..10) {
            Log.d("CountWorker", "$workName -> ${multiplicationValue * 10 + i}")
            Thread.sleep(500)
        }

        return Worker.Result.SUCCESS
    }
}