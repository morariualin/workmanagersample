package com.example.alin.workmanagersample.samples

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import androidx.work.*
import java.util.concurrent.TimeUnit

class RecurringTasksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = "Recurring Tasks"

//        PeriodicWorkRequest.Builder(CountRecurringWorker::class.java, 10, TimeUnit.SECONDS).build()
        val work = PeriodicWorkRequestBuilder<CountRecurringWorker>(10, TimeUnit.SECONDS)
                // also you can add constraints here
                .build()

        WorkManager.getInstance()?.enqueue(work)

        val workStatusObserver: Observer<WorkStatus> = Observer { workStatus ->
            if (workStatus != null) {
                Log.d("CountWorker", "work status with id ${work.id} = ${workStatus.state}")
            }
        }
        WorkManager.getInstance()?.getStatusById(work.id)
                ?.observe(this, workStatusObserver)
    }
}

class CountRecurringWorker : Worker() {

    override fun doWork(): Result {
        for (i in 0..10) {
            Log.d("CountWorker", "-> $i")
            Thread.sleep(100)
        }

        return Worker.Result.SUCCESS
    }
}